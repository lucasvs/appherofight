import { HttpClient } from '@angular/common/http';
import { Heroi } from '../objects/heroi';
import { Injectable, ɵConsole } from '@angular/core';


@Injectable({
    providedIn: 'root'
})
export class HomeService {

    constructor(private http:HttpClient){}

    public readonly URL_API="https://raw.githubusercontent.com/akabab/superhero-api/0.2.0/api/"

    private herois:Heroi[];

    public getHeroById(id:number){
        let requestUrl = this.URL_API+'all'+'.json';

        this.http.get(requestUrl).subscribe(
            resp => this.herois = resp
        )
        return this.herois;
    }
}
