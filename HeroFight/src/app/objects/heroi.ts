export class Heroi {
    id: number;
    name:String;
    slug:String;
    powerstats:{
        inteligence:number;
        strenght:number;
        speed:number;
        durability:number;
        power:number;
        combat:number;
    }
    appearance:{
        gender:String;
        race:String;
    }
    images:{
        xs:string;
        sm:string;
        md:string;
        lg:string;
    }
}
