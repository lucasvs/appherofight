import { Component, OnInit } from '@angular/core';
import { HomeService } from '../Classes/home-service';

@Component({
  selector: 'app-hero-select',
  templateUrl: './hero-select.component.html',
  styleUrls: ['./hero-select.component.sass']
})
export class HeroSelectComponent implements OnInit {

  constructor(private homeService:HomeService) { }

  BuscaHerois(){
    console.log(this.homeService.getHeroById(10));
  }

  ngOnInit() {
  }

}
